#include "CmdLine.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

FILE *LOG_F;

void print_tree(struct cli_handler *cmd_h, int level)
{
	int i;
	if(cmd_h->cli) {
		if(level < 0 || cmd_h->cli->lv <= level){
			for(i = 0; i < cmd_h->cli->lv; i++) printf("    ");
			printf("%s", cmd_h->cli->cmd);
			if(cmd_h->cli->desc) {
				printf(": %s", cmd_h->cli->desc);
			}
			printf("\n");
		}
	}else{
		// printf("root has %d parameter\n", cmd_h->num_of_child);
	}
	if(cmd_h->num_of_child) {
		for(i = 0; i < cmd_h->num_of_child; i++) {
			print_tree(&cmd_h->child[i], level);
		}
	}
}

static int findChildrenToPool(struct cli_handler *parent, 
	struct cli_handler *pool, int pool_size, 
	struct CmdLine *list, int list_len
) {
	int i,j, poolIndex, maxChildId;
	struct CmdLine *it;
	struct cli_handler *ith, tmp;

	fprintf(LOG_F, "start %s at pool %p - %d, list %p - %d\n", parent->cli->cmd, pool, pool_size, list, list_len);

	poolIndex = 0;
	for(i = 0, it = list; i < list_len; i++, it++) {
		if(it->lv == parent->cli->lv + 1) {
			ith = &parent->child[parent->num_of_child];
			ith->cli = it;
			ith->parent = parent;
			ith->child = NULL;
			ith->num_of_child = 0;

			fprintf(LOG_F, "Add %s at %p to %p\n", it->cmd, it, ith);
			parent->num_of_child++;
			poolIndex++;
		}
	}

	// fprintf(LOG_F, "For %s has %d children\n", parent->cli? parent->cli->cmd: "root", parent->num_of_child);

	for(i = 0; i < parent->num_of_child; i++) {
		if(i < parent->num_of_child - 1) {
			maxChildId = parent->child[i + 1].cli - parent->child[i].cli - 1;
		}else{
			it = parent->child[i].cli;
			maxChildId = 0;
			for(j = 1; j < list_len; j++) {
				if(it->lv >= it[j].lv) {
					break;
				}
				maxChildId++;
			}
		}
		// fprintf(LOG_F, "%s has maxChildId = %d\n", parent->child[i].cli->cmd, maxChildId );

		if(maxChildId > 0 && pool_size > 0) {
			parent->child[i].child = pool + poolIndex;
			poolIndex += findChildrenToPool(&parent->child[i], 
				parent->child[i].child, pool_size - poolIndex, 
				parent->child[i].cli + 1, maxChildId
			);
		}
	}
	return poolIndex;
}

int cli_init(struct cli_handler *cmd_h, struct CmdLine *list, int list_len)
{
	struct cli_handler *pool;
	struct CmdLine root;

	if (!LOG_F)	{
		LOG_F = fopen(__FILE__".log", "w");
		if (!LOG_F){
			printf("Failed to init %s. Use stdout\n", __FILE__".log");
			LOG_F = stdout;
		}
	}
	
	pool = (struct cli_handler*)malloc((list_len) * sizeof(struct cli_handler));
	memset(pool, 0, list_len * sizeof(struct cli_handler));

	root.lv = 0;
	root.cmd = "root";
	cmd_h->cli = &root;
	cmd_h->parent = NULL;
	cmd_h->num_of_child = 0;
	cmd_h->child = pool;

	findChildrenToPool(cmd_h, pool, list_len, list, list_len);
	cmd_h->cli = NULL;
	return 0;
}

void cli_deinit(struct cli_handler *cmd_h) {
	if(cmd_h->child && cmd_h->num_of_child) {
		free(cmd_h->child);
		cmd_h->num_of_child = 0;
	}
}

int cli_run(struct cli_handler *cmd_h, int argc, const char **argv) {
	int ret, i;
	struct cli_handler *it;



	fprintf(LOG_F, "Start %s with children=%d argc=%d, argv[0]=%s\n", __func__, cmd_h->num_of_child, argc, argc? argv[0]:"null");

	if(cmd_h->num_of_child) {
		if(argc) {
			for (i = 0, it = cmd_h->child; i < cmd_h->num_of_child; i++, it++) {
				if(it->cli) {
					if(strcmp(it->cli->cmd, argv[0]) == 0) {
						fprintf(LOG_F, "Found cli has cmd: %s\n", it->cli->cmd);
						ret = cli_run(it, argc - 1, argv + 1);
						break;
					}
				}else{
					fprintf(LOG_F, "Child does not have cli element (%d - %p)\n", i, it);
				}
			}
			if(i == cmd_h->num_of_child) {
				fprintf(LOG_F, "Cannot find children for argv: %s\n", argv[0]);

				if(cmd_h->cli) {
					if(!cmd_h->cli->check){
						fprintf(LOG_F, "%s has no checking\n", cmd_h->cli->cmd);
						printf("%d: %s only accepts one of these arguments:\n", __LINE__, cmd_h->cli->cmd);
						print_tree(cmd_h, cmd_h->cli->lv+1);
						ret = -EINVAL;
					}else{
						fprintf(LOG_F, "%s has checking\n", cmd_h->cli->cmd);
						ret = cmd_h->cli->check(argc, argv);
						if(ret > 0) {
							if(cmd_h->cli->run) {
								ret = cmd_h->cli->run(argc, argv, cmd_h->cli);
							}else{
								printf("%d: %s is not implemented yet\n", __LINE__, cmd_h->cli->cmd);
							}
						}else{
							print_tree(cmd_h, cmd_h->cli->lv + 1);
							ret = -EINVAL;
						}
					}
				}else{
					printf("%d: %s only accepts one of these arguments\n", __LINE__, argv[0]);
					print_tree(cmd_h, 1);
					ret = -EINVAL;
				}
			}else{
				ret = -EINVAL;
			}
		}else{
			if(cmd_h->cli) {
				if(cmd_h->cli->check){
					fprintf(LOG_F, "%s has checking\n", cmd_h->cli->cmd);
					ret = cmd_h->cli->check(argc, argv);
					if(ret > 0) {
						if(cmd_h->cli->run) {
							ret = cmd_h->cli->run(argc, argv, cmd_h->cli);
						}else{
							printf("%d: %s is not implemented yet\n", __LINE__, cmd_h->cli->cmd);
							ret = -ENOSYS;
						}
					}else{
						print_tree(cmd_h, cmd_h->cli->lv + 1);
						ret = -EINVAL;
					}
				}else{
					fprintf(LOG_F, "%s has no checking\n", cmd_h->cli->cmd);
					if(cmd_h->cli->run) {
						ret = cmd_h->cli->run(argc, argv, cmd_h->cli);
					}else{
						printf("%d: %s is not implemented yet\n", __LINE__, cmd_h->cli->cmd);
						ret = -ENOSYS;
					}
				}
			}else{
				printf("%d: %s needs one of these arguments\n", __LINE__, argv[0]);
				print_tree(cmd_h, 1);
				ret = -EINVAL;
			}
		}
	}else{
		if(argc) {

			if(cmd_h->cli) {
				if(cmd_h->cli->check){
					ret = cmd_h->cli->check(argc, argv);
					if(ret > 0) {
						if(cmd_h->cli->run) {
							ret = cmd_h->cli->run(argc, argv, cmd_h->cli);
						}else{
							printf("%d: %s is not implemented yet\n", __LINE__, cmd_h->cli->cmd);
							ret = -ENOSYS;
						}
					}else{
						print_tree(cmd_h, cmd_h->cli->lv + 1);
						ret = -EINVAL;
					}
				}else{
					printf("%d: %s does not accept any argument\n", __LINE__, cmd_h->cli->cmd);
					ret = -EINVAL;
				}
			}else{
				printf("%d: %s does not work\n", __LINE__, argv[0]);
				ret = -EINVAL;
			}
		}else{

			if(cmd_h->cli) {
				if(cmd_h->cli->check){
					ret = cmd_h->cli->check(argc, argv);
					if(ret > 0) {
						if(cmd_h->cli->run) {
							ret = cmd_h->cli->run(argc, argv, cmd_h->cli);
						}else{
							printf("%d: %s is not implemented yet\n", __LINE__, cmd_h->cli->cmd);
							ret = -ENOSYS;
						}
					}else{
						print_tree(cmd_h, cmd_h->cli->lv + 1);
						ret = -EINVAL;
					}
				}else{
					if(cmd_h->cli->run) {
						ret = cmd_h->cli->run(argc, argv, cmd_h->cli);
					}else{
						printf("%d: %s is not implemented yet\n", __LINE__, cmd_h->cli->cmd);
						ret = -ENOSYS;
					}
				}
			}else{
				printf("%d: %s does not work\n", __LINE__, argv[0]);
				print_tree(cmd_h, 1);
				ret = -EINVAL;
			}
		}
	}
	return ret;
}