struct CmdLine{
	const char *cmd;
	int (*run)(int, const char**, const void*);
	int (*check)(int, const char**);
	const char *desc;
	int lv;
};

struct cli_handler{
	struct CmdLine *cli;
	struct cli_handler *parent;
	struct cli_handler *child;
	int num_of_child;
};

int cli_init(struct cli_handler *cmd_h, struct CmdLine *list, int list_len);
void cli_deinit(struct cli_handler *cmd_h);
// int cli_traverse(struct CmdLine *list, int list_len, int *index, int level);
int cli_run(struct cli_handler *cmd_h, int argc, const char **argv);

void print_tree(struct cli_handler *cmd_h, int level);