OBJ = CmdLine.o cli.o check.o
HEAD = CmdLine.h


TARGET := cli

$(TARGET): $(OBJ) $(HEAD)
	$(CC) $^ -I. -o $@

clean:
	-rm -rf *.o $(TARGET) *.log
