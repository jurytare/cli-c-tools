# CLI template for user interaction program

### Building
Using this command to build:
```shell
cd <directory of repo>
make
```
### Usage
The program has 3 features:
- `cli help` will show the help menu
- `cli version` will show the version of software
- `cli run hello world` will show `hello world`

Otherwise, it will print the list of commands
