#include <stdlib.h>
#include <stdio.h>

int must_argv(int argc, const char **argv)
{
    if(argc) {
        return 1;
    }else{
        printf("Missing argument\n");
        return 0;
    }
}

int any_argv(int argc, const char **argv)
{
    return 1;
}

int num_argv(int argc, const char **argv)
{
    char *pEnd;
    long int ret;
    if(argc){
        ret = strtol(argv[0], &pEnd, 10);
        if(pEnd != argv[0]) {
            return 1;
        }else{
            printf("%s is not a number\n", argv[0]);
            return 0;
        }
    }else{
        printf("Missing a number as argument\n");
        return 0;
    }
}
