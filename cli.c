#include "CmdLine.h"
#include <stdlib.h>
#include <time.h>
#include <stdio.h>

FILE *LOG_F;
struct cli_handler cmd_h;

int print_help(int argc, const char **argv, const void *sub_args);
int print_help_all(int argc, const char **argv, const void *sub_args);

int print_helloworld(int argc, const char **argv, const void *sub_args);
int print_hello(int argc, const char **argv, const void *sub_args);
int print_desc(int argc, const char **argv, const void *sub_args);

extern int any_argv(int argc, const char **argv);
extern int must_argv(int argc, const char **argv);
extern int num_argv(int argc, const char **argv);

struct CmdLine cli[] = {

{"help", print_help, NULL, "Show short usages instructions", 1},
    {"all", print_help_all, NULL, "Show all usages instructions", 2},

{"pa", print_desc, NULL, "Command without any arguments", 1},
{"pap", print_desc, any_argv, "Comand accepts all arguments", 1},

{"pach", NULL, NULL, NULL, 1}, // Command accepts children only
    {"ch", print_desc, NULL, "Child without any arguments", 2},
    {"chp", print_desc, any_argv, "Child accepts all arguments", 2},

{"pachp", print_desc, num_argv, "Command accepts children or number as argument", 1},
    {"ch", print_desc, NULL, "Child without any arguments", 2},

{"pachg", print_desc, NULL, "Command accepts children or no argument", 1},
    {"ch", print_desc, must_argv, "Child must be with an argument", 2},


{"run", NULL, NULL, "Execute an example", 1},
	{"hello", print_hello, NULL, "Execute an example", 2},
		{"world", print_helloworld, NULL, "Execute an example", 3},
};

int main(int argc, char const *argv[])
{
	int len, ret;

    LOG_F = fopen("/tmp/log.txt", "w");
	len = sizeof(cli)/sizeof(struct CmdLine);

	cli_init(&cmd_h, cli, len);
	ret = cli_run(&cmd_h, argc - 1, argv + 1);
    cli_deinit(&cmd_h);

	return ret;
}

int print_desc(int argc, const char **argv, const void *sub_args)
{
    const struct CmdLine *cli = sub_args;
    printf("%s: %s (param: %s)\n", cli->cmd, cli->desc, argc > 0 ? argv[0] : "null");
    return 0;
}

int print_help_all(int argc, const char **argv, const void *sub_args)
{
    print_tree(&cmd_h, -1);
    return 0;
}

int print_help(int argc, const char **argv, const void *sub_args)
{
    print_tree(&cmd_h, 1);
    return 0;
}

int print_hello(int argc, const char **argv, const void *sub_args)
{
    printf("Hello from child %s\n", *(argv-1));
    return 0;
}

int print_helloworld(int argc, const char **argv, const void *sub_args)
{
    printf("Hello world\r\n");
    return 0;
}
